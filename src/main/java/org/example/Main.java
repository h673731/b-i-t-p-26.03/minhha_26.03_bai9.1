package org.example;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vào một số nguyên trong khoảng từ 1 đến 100: ");
        int soNguyen = scanner.nextInt();

        GuessNumberThread thread1 = new GuessNumberThread("Thread 1: ", soNguyen);
        GuessNumberThread thread2 = new GuessNumberThread("Thread 2: ", soNguyen);

        thread1.start();
        thread2.start();
    }
}

class GuessNumberThread extends Thread {
    private int soCanDoan;
    public GuessNumberThread(String name, int soCanDoan) {
        super(name);
        this.soCanDoan = soCanDoan;
    }
    public void run() {
        Random random = new Random();
        int soLanDem = 0;
        while (true) {
            int soLanDoan = random.nextInt(100) + 1;
            soLanDem++;
            System.out.println(getName() + "Lần đoán thứ " + soLanDem + ": " + soLanDoan);
            if (soLanDoan == soCanDoan) {
                System.out.println(getName() + "Bạn đã đoán đúng số " + soCanDoan + " sau " + soLanDem + " lần");
                break;
            }
            else {
                System.out.println(getName() + "Đoán sai, đoán lại");
            }
        }
    }
}